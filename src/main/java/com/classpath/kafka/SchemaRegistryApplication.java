package com.classpath.kafka;

import java.io.IOException;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.classpath.kafka.config.KafkaConfig;

import io.confluent.kafka.schemaregistry.client.rest.exceptions.RestClientException;

@SpringBootApplication
public class SchemaRegistryApplication {

	@Autowired
	private KafkaConfig kafkaConfig;

	public static void main(String[] args) {
		SpringApplication.run(SchemaRegistryApplication.class, args);
	}

	@PostConstruct
	public void init() throws IOException, RestClientException {
		kafkaConfig.registerSchema();
	}
}
