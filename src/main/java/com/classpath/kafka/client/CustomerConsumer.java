package com.classpath.kafka.client;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.classpath.kafka.model.Customer;

@Service
public class CustomerConsumer {

    private static final String TOPIC = "customer";

    @KafkaListener(topics = TOPIC, groupId = "customer-group")
    public void receive(Customer customer) {
        System.out.println("Received customer: " + customer);
    }
}
