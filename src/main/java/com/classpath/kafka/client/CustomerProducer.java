package com.classpath.kafka.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import com.classpath.kafka.model.Customer;
import com.github.javafaker.Faker;

@Component
public class CustomerProducer implements CommandLineRunner {

	private static final String TOPIC = "customer";

	@Autowired
	private KafkaTemplate<String, Customer> kafkaTemplate;
	
	private final Faker faker = new Faker();

	public void send(Customer customer) {
		kafkaTemplate.send(TOPIC, customer.getId()+"", customer);
	}

	@Override
	public void run(String... args) throws Exception {

		for (int i = 0; i < 100; i++) {
			Thread.sleep(2000);
			Customer customer = new Customer();
			customer.setId(faker.number().randomDigit());
			customer.setName(faker.name().firstName());
			customer.setAge(faker.number().numberBetween(20, 50));
			customer.setEmail(faker.name().firstName()+ "@"+faker.internet().domainName());

			this.send(customer);
		}

	}
}
