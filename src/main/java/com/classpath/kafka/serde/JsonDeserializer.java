package com.classpath.kafka.serde;

import java.io.IOException;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import com.classpath.kafka.model.Customer;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonDeserializer implements Deserializer<Customer> {

    private final ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public Customer deserialize(String topic, byte[] data) {
        if (data == null) {
            return null;
        }
        try {
            return objectMapper.readValue(data, Customer.class);
        } catch (IOException e) {
            throw new SerializationException("Error deserializing Customer from JSON", e);
        }
    }
}

